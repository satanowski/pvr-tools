"""Filmweb API simplistic wrapper.

Copyright (C) 2016 Satanowski <satanowski@gmail.com>
License: GNU AGPLv3
"""

from datetime import datetime, timedelta
import json
import re

from pyquery import PyQuery as pq  # noqa: N813
import colorlog


handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(levelname)s:'
                                               '%(name)s:%(message)s'))
log = colorlog.getLogger('Filmweb')
log.setLevel(colorlog.colorlog.logging.DEBUG)
log.addHandler(handler)


class Filmweb():
    """Simplistic wrapper for Filmweb's "API"."""

    API = "http://www.filmweb.pl/tvChannelGuide/{:d}?day={:d}"
    CHANNELS = {
        1: 'TVP 1',
        2: 'TVP 2',
        3: 'Polsat',
        4: 'TVN',
        5: 'TVN 7',
        27: 'TV 4',
        28: 'TV Puls',
        36: 'TVP Kultura',
        63: 'TTV',
        64: 'TVP Historia',
        78: 'TV 6',
        94: 'Puls 2',
        108: 'TVP Rozrywka',
        110: 'ATM Rozrywka',
        117: 'Stopklatka TV',
        120: 'Fokus TV'
    }

    def __init__(self):
        """Lorem ipsum."""
        self.cache = {}

    def _get_data(self, url):
        """Retrieve given url and extract EPG data."""
        try:
            log.debug('Getting URL %s', url)
            pd = pq(url)
        except ConnectionError:  # noqa: F821
            log.error('Cannot retrieve: %s', url)

        if not pd:
            return None

        # get starting times and type from HTML part
        start_data = {}
        for s in pd.find('.seance'):
            sid = s.get('data-sid')
            start_data[sid] = {}
            start_data[sid].update(
                {f: s.get('data-' + f) for f in ['type', 'start', 'film']}
            )
            start_data[sid].update(
                {'title': s.find_class('sd')[0].text_content().strip()}
            )

            # convert start time string to datetime object
            start_data[sid]['start'] = datetime(*[
                int(x) for x in start_data[sid]['start'].split(',')
            ])

        # get JSON part
        j = pd.find('.seanceInfoJSON')
        if not j or not j[0].text:
            return None

        seanses = json.loads(j[0].text)

        # get duration and synopsis from JSON part
        fields = ['type', 'duration', 'synopsis']
        data = {
            sid: {f: s.get(f) for f in fields} for sid, s in
            seanses.get('seances', {}).items()
        }

        # append data from "start_data" and convert "duration" to timedelta
        for d in data:
            data[d].update(start_data[d])
            s = re.search(
                '((\d+)\s*godz\.)*\s*(\d+)\s*min\.',
                data[d]['duration']
            )
            if s:
                skip, h, m = s.groups()
                data[d]['duration'] = timedelta(
                    hours=int(h) if h else 0,
                    minutes=int(m) if m else 0
                )
            else:
                data[d]['duration'] = timedelta(0)

        return data

    def get_program(self, channel, day, filter_type=None):
        """Get program for given channel and day.

        day = 0 means today, day = 1 means tommorow, and so on.
        """
        if not self.cache.get(channel, {}).get(day):
            self.cache[channel] = {}
            data = self._get_data(self.API.format(channel, day))
            if not data:
                log.error('No data for channel %s for day %d',
                          self.CHANNELS[channel, day])
            self.cache[channel][day] = data or {}
        if filter_type:
            fltrd = filter(
                lambda x: self.cache[channel][day][x]['type'] == filter_type,
                self.cache[channel][day].keys()
            )
            return {k: self.cache[channel][day][k] for k in fltrd}

        return self.cache[channel][day]

    def get_all_for_day(self, day):
        """Get EPG info for all channels for given day."""
        for channel in self.CHANNELS:
            self.get_program(channel, day)


if __name__ == '__main__':
    fw = Filmweb()
    fw.get_all_for_day(0)
