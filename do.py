#!/usr/bin/env python3
from datetime import datetime
from collections import namedtuple
from tempfile import NamedTemporaryFile as ntf
from subprocess import run, PIPE
from glob import glob
import sys
import os
import re

import begin


class DrsMovieCleaner():
 
    CMD_SPLIT = "{ffmpeg} -y -i {movie} -vcodec copy -acodec copy -scodec copy " \
                "{mapping}  -metadata:s:s:0 language=pol " \
                "-ss {start} -t {duration} {output}"

    CMD_CNCAT = "{ffmpeg} -f concat -i {listfile} -c:v copy -c:a copy -c:s copy" \
                " -map 0 -metadata:s:s:0 language=pol {output}"

    MP_FILE = 'movie_part'

    Time = namedtuple('Time', "h,m,s")

    def __init__(self, movie_file=None, times_file=None):
        self.times = []
        self.streams = []
        self.movie = movie_file
        self.times_file = times_file
        self.parts = []

        for prog in ['ffmpeg', 'ffprobe']:
            r = run(['which', prog], stdout=PIPE)
            if r.returncode:
                sys.exit('No {}!'.format(prog.upper()))
            setattr(self, prog, r.stdout.decode().strip())


    def read_times(self):
        """Read times from text file."""
        with open(self.times_file, 'r') as f:
            lines = map(str.strip, f.readlines())

        for line in filter(lambda x:not x.startswith('#'), lines):
            times = re.findall('(\d+:\d+:\d+)\ *-\ *(\d+:\d+:\d+)', line)
            if not times:
                continue
            start = [int(x) for x in times[0][0].split(':')]
            stop = [int(x) for x in times[0][1].split(':')]
            fake_date = (1970, 1, 1)
            start = datetime(*fake_date, *start)
            dur = datetime(*fake_date, *stop) - start
            dur_h = dur.seconds // 3600
            dur_m, dur_s = divmod(dur.seconds-(dur.seconds//3600)*3600, 60)

            self.times.append((
                self.Time(start.hour, start.minute, start.second),
                self.Time(dur_h, dur_m, dur_s)
            ))


    def probe(self):
        """Select interesting streams in given movie file."""
        def f(s):
            nono = ['visual impaired', 'Unknown:', 'dvb_teletext']
            if s.find('Stream') == -1:
                return False
            if any(s.find(no)>=0 for no in nono):
                return False
            return True
    
        r = run([self.ffprobe, self.movie], stderr=PIPE)
        if r.returncode:
            self.streams = []
        
        for stream in filter(f, r.stderr.decode().split('\n')):
            x = re.search("Stream\ \#0:(\d)", stream)
            if x:
                self.streams.append(x.group(1))


    def _part_name(self):
        num = 1
        temp_name = '{}_{}.mts'.format(self.MP_FILE, num)
        while os.path.exists(temp_name):
            num += 1
            temp_name = '{}_{}.mts'.format(self.MP_FILE, num)
    
        return temp_name


    def split(self):
        self.read_times()
        self.probe()
        mapping = ' '.join(('-map 0:{}'.format(d) for d in self.streams))

        for start, dur in self.times:
            part_file = self._part_name()   

            cmd = self.CMD_SPLIT.format(
                ffmpeg=self.ffmpeg,
                movie=self.movie,
                start="{}:{}:{}".format(start.h, start.m, start.s),
                duration="{}:{}:{}".format(dur.h, dur.m, dur.s),
                output=part_file,
                mapping=mapping
            )
            print(cmd)
            self.parts.append(part_file)
            run(cmd.split()).returncode == 0

    def concat(self):
        part_file = 'part_list'
        parts = self.parts or sorted(glob('{}_*'.format(self.MP_FILE)))

        with open(part_file, 'w') as f:
            f.write(
                '\n'.join(map(lambda x:'file '+x, parts))
            )

        cmd = self.CMD_CNCAT.format(
            ffmpeg=self.ffmpeg,
            listfile=part_file,
            output="OUT_{}".format(self.movie or 'MOVIE.mts')
        )
        print(cmd)
        return run(cmd.split()).returncode == 0


@begin.start
def main(cc=True, justcc=False, *args):
    if justcc: # movie and times files not needed
        mc = DrsMovieCleaner()
        mc.concat() 
    else:
        if len(args) != 2:
            sys.exit("Wrong arguments!")
        
        if any(not os.path.exists(f) for f in args):
            sys.exit('Wrong path given!')

        mc = DrsMovieCleaner(args[0], args[1])
        mc.split()
        if cc and len(mc.parts) > 1:
            mc.concat()
